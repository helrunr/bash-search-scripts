#!/bin/bash
# Usage:
# bash systeminfo.sh < syscoms.txt
# 
# syscoms.txt is file you need to provide. It should declare the system commands
# you wish to run on the target system and provide an xml tag for each.
# An example file is included in this repo.

function SepCmds()
{
	LCMD=${ALINE%%|*}
	REST=${ALINE#*|}
	WCMD=${REST%%|*}
	REST=${REST#*|}
	TAG=${REST%%|*}

	if [[ $OSTYPE == "MSWin" ]]
	then
		CMD="$WCMD"
	else
		CMD="$LCMD"
	fi
}

function DumpInfo()
{
	printf '<systeminfo hosts="%s" type="%s"' "$HOSTNAME" "$OSTYPE"
	printf ' data="%s" time="%s">\n' "$(date '+%F')" "$(date '+%T')"

	readarray CMDS

	for ALINE in "${CMDS[@]}"
	do
		# ignore comments
		if [[ ${ALINE:0:1} == '#' ]] ; then continue ; fi

		SepCmds

		if [[ ${CMD:0:3} == N/A ]]
		then
			continue
		else
			printf "<%s>\n" $TAG
			$CMD
			printf "</%s>\n" $TAG
		fi
	done
	printf "</systeminfo>\n"
}

# Invoke script to detect operating system
OSTYPE=$(./getos.sh)

# Run the hostname program in a subshell to retrieve the
# name of the system
HOSTNM=$(hostname)

# The hostname is used as part of the temporary filename which 
# will store all the output
TMPFILE="${HOSTNM}.info"

# gather the info into the tmp file; errors, too
DumpInfo > $TMPFILE 2>&1
